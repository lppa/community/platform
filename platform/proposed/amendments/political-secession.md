'Free and Open Government':

We recognize the right to political secession. This includes the right of secession by political entities, private groups, and individuals. Exercise of this right, like the exercise of all rights, does not remove legal and moral obligations not to violate the rights of others. Those who wish to secede should not have to obtain the permission of those from whom they wish to secede from."
